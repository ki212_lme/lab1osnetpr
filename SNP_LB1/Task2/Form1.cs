using System.IO.MemoryMappedFiles;
using System.Text.RegularExpressions;

namespace Task2_CSharp_;

public partial class Form1 : Form
{
    private const int AmountNumber = 30;
    private Mutex mtx;

    public Form1()
    {
        InitializeComponent();
        timer1.Interval = 500;
        mtx = new Mutex();
    }

    private void button1_Click(object sender, EventArgs e)
    {
        timer1.Enabled = !timer1.Enabled;
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
        try
        {
            mtx.WaitOne();
            using (var mappedFile = MemoryMappedFile.OpenExisting("data"))
            {
                using (Stream view = mappedFile.CreateViewStream())
                {
                    TextBox1.Text = "";
                    var binReader = new BinaryReader(view);
                    var text = System.Text.Encoding.UTF8.GetString(binReader.ReadBytes((int)view.Length)).ToString();
                    var regex = new Regex(@"\d+");
                    var matches = regex.Matches(text);
                    var numbs = new int[AmountNumber];
                    for (var i = 0; i < numbs.Length; i++) numbs[i] = int.Parse(matches[i].Value);
                    for (var i = 0; i < numbs.Length; i++)
                    {
                        text = "";
                        text += (i + 1).ToString() + ". ";
                        for (var j = 0; j < numbs[i]; j++) text += '*';
                        TextBox1.AppendText(text);
                        TextBox1.AppendText(Environment.NewLine);
                    }
                }
            }

            mtx.ReleaseMutex();
        }
        catch
        {
            TextBox1.Text = "";
            TextBox1.AppendText("Cannot get data from myFile!" + "\r\n");
        }
    }

    private void button2_Click(object sender, EventArgs e)
    {
        TextBox1.Text = "";
        mtx.Dispose();
    }
}