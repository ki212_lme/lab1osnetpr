#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <sstream>
#include <iostream>
#include <regex>
#include <mutex>s
using namespace std;
#define AmountNumb 30
TCHAR szName[] = TEXT("data");
mutex mtx;
void SortMapFile(HANDLE hFile,LPCTSTR pBuf) {
    mtx.lock();
    cout << (char*)pBuf << endl;
    Sleep(800);
    string text((char*)pBuf);
    smatch sm;
    regex str_exp(R"(\d+)");
    int allnumb[AmountNumb];
    int f = 0;
    while (regex_search(text, sm, str_exp)) {
        allnumb[f] = stoi((string)sm.str(0));
        text = sm.suffix().str();
        f += 1;
    }
    int j = 0;
    int tmp = 0;
    for (int i = 0; i < f; i++) {
        j = i;
        for (int k = i; k < f; k++) {
            if (allnumb[j] > allnumb[k]) {
                j = k;
            }
        }
        tmp = allnumb[i];
        allnumb[i] = allnumb[j];
        allnumb[j] = tmp;
        string replace1 = "";
        for (int i = 0; i < f; i++) {
            stringstream ss;
            ss << allnumb[i];
            replace1 += ss.str() + " ";
        }
        char replace2[200];
        strcpy_s(replace2, replace1.c_str());
        CopyMemory((PVOID)pBuf, replace2, sizeof(replace2));
        WriteFile(hFile, replace2, (DWORD)sizeof(replace2), NULL, NULL);
        Sleep(500);
    }
    printf("Array is sorted\n");
    mtx.unlock();
}
int main()
{
    HANDLE hMapFile, hFile;
    LPCTSTR pBuf;

    hFile = CreateFile(TEXT("data.dat"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, NULL);                 
    hMapFile = CreateFileMapping(hFile, NULL, PAGE_READWRITE, NULL, NULL, szName);                 
    if (hMapFile == NULL)
    {
        _tprintf(TEXT("Could not create file mapping object (%d).\n"),
            GetLastError());
        return 1;
    }
    pBuf = (LPTSTR)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, NULL, NULL, NULL);
    if (pBuf == NULL)
    {
        _tprintf(TEXT("Could not map view of file (%d).\n"),
            GetLastError());
        CloseHandle(hMapFile);
        return 1;
    }
    bool ok = TRUE;
    printf("PRESS SPACE KEY TO BEGIN SORTING\n");
    do {
        if (GetKeyState(VK_UP) & 0x8000) {
            printf("END\n");
            ok = FALSE;
        }
        if (GetKeyState(VK_SPACE) & 0x8000)
        {
            printf("SPACE KEY PRESSED\n"); 
            SortMapFile(hFile, pBuf);
        }
    } while (ok);
    cout << endl << "Process is complete" << endl;
    string b;
    cin >> b;
    UnmapViewOfFile(pBuf);
    CloseHandle(hMapFile);
    CloseHandle(hFile);
    return 0;
}