#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <sstream>
#include <iostream>
#include <mutex>
using namespace std;
#define AmountNumb 30
TCHAR szName[] = TEXT("data");
mutex mtx;
void GenerateArray(LPCTSTR pBuf) {
    mtx.lock();
    int allnumb[AmountNumb];
    for (int i = 0; i < AmountNumb; i++) {
        allnumb[i] = 10 + (rand() % 91);
    }
    string replace1 = "";
    for (int i = 0; i < AmountNumb; i++) {
        stringstream ss;
        ss << allnumb[i];
        replace1 += ss.str() + " ";
    }
    char replace2[200];
    strcpy_s(replace2, replace1.c_str());
    CopyMemory((PVOID)pBuf, replace2, sizeof(replace2));
    printf("Array generated\n\n");
    Sleep(500);
    mtx.unlock();
}
int main()
{
    HANDLE hMapFile;
    LPCTSTR pBuf;
    bool ok = TRUE;
    printf("Start process PRESS DOWN ARROW KEY\n");
    do {
        if (GetKeyState(VK_DOWN) & 0x8000)
        {
            Sleep(800);
            ok = FALSE;
        }
    } while (ok);

    hMapFile = OpenFileMapping(
        FILE_MAP_ALL_ACCESS,   
        FALSE,                 
        szName);               
    if (hMapFile == NULL)
    {
        _tprintf(TEXT("Could not open file mapping object (%d).\n"),
            GetLastError());
        return 1;
    }

    pBuf = (LPTSTR)MapViewOfFile(hMapFile, 
        FILE_MAP_ALL_ACCESS,  
        0,
        0,
        NULL);
    if (pBuf == NULL)
    {
        _tprintf(TEXT("Could not map view of file (%d).\n"),
            GetLastError());
        CloseHandle(hMapFile);
        return 1;
    }    
    printf("EVERY PRESS DOWN ARROW KEY GENERATE ARRAY\n");
    ok = TRUE;
    do {
        if (GetKeyState(VK_UP) & 0x8000) {
            printf("END\n");
            printf("PROCESS DOWN\n\n");
            ok = FALSE;
        }
        if (GetKeyState(VK_DOWN) & 0x8000)
        {
            printf("DOWN ARROW KEY PRESSED\n");
            GenerateArray(pBuf);
        }
    } while (ok);
    string b;
    cin >> b;
    UnmapViewOfFile(pBuf);
    CloseHandle(hMapFile);
    return 0;
}